
import React from 'react';
import './App.css';
import  {ExampleComponentChild1} from 'aschild1'

function App() {
  return (
    <ExampleComponentChild1 showChild2={true} />
  );
}

export default App;
